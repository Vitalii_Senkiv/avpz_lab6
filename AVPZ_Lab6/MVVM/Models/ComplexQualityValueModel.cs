﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab6.MVVM.Models
{
    class ComplexQualityValueModel
    {
        public string QualityCreteria { get; set; }

        public double BranchExpertMark { get; set; }
        public double UsabilityExpertMark { get; set; }
        public double ProgrammingExpertMark { get; set; }
        public double UserMark { get; set; }

        public double AverageMark { get; set; }
    }
}

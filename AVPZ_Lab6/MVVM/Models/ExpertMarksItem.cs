﻿using AVPZ_Lab6.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab6.MVVM.Models
{
    internal class ExpertMarksItem
    {
        public int Id { get; set; }

        public int AreaExpertMark { get; set; }
        public int UsabilityExpertMark { get; set; }
        public int ProgrammingExpertMark { get; set; }
        public double PotentialUserMark { get; set; }

        public void RandomizeMarks(int UsersMarksSum)
        {
            Random rnd = new Random();
            AreaExpertMark = rnd.Next(5, 10);
            UsabilityExpertMark = rnd.Next(5, 10);
            ProgrammingExpertMark = rnd.Next(5, 10);
            PotentialUserMark = Math.Round(UsersMarksSum / 20d, 2);
        }
    }
}

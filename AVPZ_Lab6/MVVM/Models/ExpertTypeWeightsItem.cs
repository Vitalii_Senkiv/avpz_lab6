﻿using AVPZ_Lab6.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab6.MVVM.Models
{
    public class ExpertTypeWeightsItem
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int WeightCoef { get; set; }
        public double RelativeWeightCoef { get; set; }

        public void RandomizeCoefficients()
        {
            Random rnd = new Random();

            WeightCoef = rnd.Next(5, 10);
            RelativeWeightCoef = WeightCoef / 10d;
        }
    }
}

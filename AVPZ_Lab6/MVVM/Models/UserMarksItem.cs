﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace AVPZ_Lab6.MVVM.Models
{
    public class UserMarksItem
    {
        public int Id { get; set; }
        public int[] UserMarks { get; set; } = new int[20];

        public void RandomizeMarks()
        {
            Random rnd = new Random();

            for (int i = 0; i < UserMarks.Length; i++)
            {
                UserMarks[i] = rnd.Next(3, 10);
            }
        }

        public int GetMarksSum()
        {
            int sum = 0;
            for (int i = 0; i < UserMarks.Length; i++)
            {
                sum += UserMarks[i];
            }
            return sum;
        }
    }
}

﻿using AVPZ_Lab6.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab6.MVVM.Models
{
    public class WeightCoefficientsItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ExpertCoef { get; set; }
        public int UsabilityExpertCoef { get; set; }
        public int ProgrammingExpertCoef { get; set; }
        public int PotentialUserCoef { get; set; }

        public void RandomizeExpertCoefficients()
        {
            Random rnd = new Random();
            ExpertCoef = rnd.Next(5, 10);
            UsabilityExpertCoef = rnd.Next(5, 10);
            ProgrammingExpertCoef = rnd.Next(5, 10);
            PotentialUserCoef = rnd.Next(5, 10);
        }
        public double GetAverage()
        {
            return ExpertCoef + UsabilityExpertCoef + ProgrammingExpertCoef + PotentialUserCoef / 4d;
        }
    }
}

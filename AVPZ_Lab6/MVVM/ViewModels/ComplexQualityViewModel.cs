﻿using AVPZ_Lab6.MVVM.Models;
using AVPZ_Lab6.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AVPZ_Lab6.MVVM.ViewModels
{
    internal class ComplexQualityViewModel : ViewModelBase
    {
        private ObservableCollection<ComplexQualityValueModel> _complexQualityValueList;
        public ObservableCollection<ComplexQualityValueModel> ComplexQualityValueList
        {
            get => _complexQualityValueList;
            set => Set(ref _complexQualityValueList, value);
        }

        MainViewModel _mainViewModel;

        public ComplexQualityViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;

            ComplexQualityValueList = new ObservableCollection<ComplexQualityValueModel>();
            #region Setup ComplexQualityViewModelList
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Точність управління та обчислень" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Ступінь стандартності інтерфейсів" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Функціональна повнота" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Стійкість до помилок" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Можливість розширення" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Зручність роботи" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Простота роботи" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Відповідність чинним стандартам" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Переносимість між ПЗ" });
            ComplexQualityValueList.Add(new ComplexQualityValueModel { QualityCreteria = "Зручність навчання" });
            #endregion

            CalculateTable();
        }

        public void CalculateTable()
        {
            var ExpertTypeWeight = _mainViewModel.ExpertTypeWeights;
            double relativeCoefSum = ExpertTypeWeight.Select(x => x.RelativeWeightCoef).Sum();

            for (int i = 0; i < ComplexQualityValueList.Count; ++i)
            {
                var WeightCoefficients = _mainViewModel.WeightCoefficients[i];
                var ExpertsMarks = _mainViewModel.ExpertMarks[i];

                var ComplexQualityValue = ComplexQualityValueList[i];

                ComplexQualityValue.BranchExpertMark = Math.Round((double)WeightCoefficients.ExpertCoef * ExpertsMarks.AreaExpertMark, 2);
                ComplexQualityValue.UsabilityExpertMark = Math.Round((double)WeightCoefficients.UsabilityExpertCoef * ExpertsMarks.UsabilityExpertMark, 2);
                ComplexQualityValue.ProgrammingExpertMark = Math.Round((double)WeightCoefficients.ProgrammingExpertCoef * ExpertsMarks.ProgrammingExpertMark, 2);
                ComplexQualityValue.UserMark = Math.Round(WeightCoefficients.PotentialUserCoef * ExpertsMarks.PotentialUserMark, 2);

                double sum = ComplexQualityValue.BranchExpertMark * ExpertTypeWeight[0].RelativeWeightCoef
                    + ComplexQualityValue.UsabilityExpertMark * ExpertTypeWeight[1].RelativeWeightCoef
                    + ComplexQualityValue.ProgrammingExpertMark * ExpertTypeWeight[2].RelativeWeightCoef
                    + ComplexQualityValue.UserMark * ExpertTypeWeight[3].RelativeWeightCoef;

                sum /= relativeCoefSum;

                ComplexQualityValue.AverageMark = Math.Round(sum, 2);
            }
        }
    }
}

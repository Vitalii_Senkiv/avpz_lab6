﻿using AVPZ_Lab6.MVVM.ViewModels.Base;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace AVPZ_Lab6.MVVM.ViewModels
{
    internal class GraphicsViewModel : ViewModelBase
    {
        MainViewModel _mainViewModel;

        public ObservableCollection<PlotModel> PlotModels { get; set; }
        public string[] AreaStrings { get; set; } = new string[5];

        public GraphicsViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;

            InitializePlotModels();

            UpdateCharts();
        }

        private void InitializePlotModels()
        {
            if (PlotModels == null)
            {
                PlotModels = new ObservableCollection<PlotModel>();
            }
            else
            {
                PlotModels.Clear();
            }

            for (int i = 0; i < 6; i++)
            {
                var pm = new PlotModel()
                {
                    PlotAreaBorderColor = OxyColors.White,
                    Padding = new OxyThickness(5),
                    PlotMargins = new OxyThickness(5),
                };
                pm.PlotType = PlotType.Polar;
                pm.Axes.Add(new AngleAxis()
                {
                    Minimum = 0,
                    Maximum = 360,
                    MajorStep = 36,
                    StartAngle = 0,
                });

                pm.Axes.Add(new MagnitudeAxis()
                {
                    MajorGridlineStyle = LineStyle.Solid,
                    MajorGridlineThickness = 2,
                    MinorGridlineStyle = LineStyle.Solid,
                    Minimum = 0,
                    Maximum = 100,
                    MajorStep = 20,
                    MinorStep = 10,
                });

                PlotModels.Add(pm);
            }
        }

        public void UpdateCharts()
        {
            PlotModels[0].Series.Clear();
            PlotModels[0].Series.Add(CalculateBranchExpertChart());

            PlotModels[1].Series.Clear();
            PlotModels[1].Series.Add(CalculateUsabilityExpertChart());

            PlotModels[2].Series.Clear();
            PlotModels[2].Series.Add(CalculateProgrammingExpertChart());

            PlotModels[3].Series.Clear();
            PlotModels[3].Series.Add(CalculateUsersChart());

            PlotModels[4].Series.Clear();
            PlotModels[4].Series.Add(CalculateAverageChart());
        }

        private AreaSeries CalculateBranchExpertChart()
        {
            var values = new AreaSeries() { Title = "Галузі" };

            int sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += _mainViewModel.WeightCoefficients[i].ExpertCoef;
            }

            double a, b, aprev = 0, bprev = 0;
            double area = 0;
            double currAngles = 0 - ((double)_mainViewModel.WeightCoefficients[0].ExpertCoef / sum) * 360 / 2;
            
            for (int i = 0; i < 10; ++i)
            {
                double angle = ((double)_mainViewModel.WeightCoefficients[i].ExpertCoef / sum) * 360;
                double value = 
                    _mainViewModel.WeightCoefficients[i].ExpertCoef 
                    * _mainViewModel.ExpertMarks[i].AreaExpertMark 
                    * _mainViewModel.ExpertTypeWeights[0].RelativeWeightCoef;

                double nextangle = currAngles += angle;
                angle = (currAngles + nextangle) / 2;

                a = Math.Sin((Math.PI / 180) * angle) * value;
                b = Math.Cos((Math.PI / 180) * angle) * value;
                if (i != 0)
                {
                    area += Math.Abs(aprev * b - bprev * a) / 2;
                }

                aprev = a;
                bprev = b;
                values.Points.Add(new DataPoint(value, angle));
                currAngles = nextangle;
            }

            AreaStrings[0] = "Площа: " + Math.Round(area, 2);
            values.Fill = OxyColor.FromArgb(200, 202, 210, 197);
            values.Points.Add(values.Points[0]);
            return values;
        }
        private AreaSeries CalculateUsabilityExpertChart()
        {
            var values = new AreaSeries() { Title = "Юзабіліті" };

            int sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += _mainViewModel.WeightCoefficients[i].UsabilityExpertCoef;
            }

            double a, b, aprev = 0, bprev = 0;
            double area = 0;
            double currAngles = 0 - ((double)_mainViewModel.WeightCoefficients[0].UsabilityExpertCoef / sum) * 360 / 2;
            for (int i = 0; i < 10; ++i)
            {
                double angle = ((double)_mainViewModel.WeightCoefficients[i].UsabilityExpertCoef / sum) * 360;
                double value = 
                    _mainViewModel.WeightCoefficients[i].UsabilityExpertCoef 
                    * _mainViewModel.ExpertMarks[i].UsabilityExpertMark 
                    * _mainViewModel.ExpertTypeWeights[1].RelativeWeightCoef;

                double nextangle = currAngles += angle;
                angle = (currAngles + nextangle) / 2;

                a = Math.Sin((Math.PI / 180) * angle) * value;
                b = Math.Cos((Math.PI / 180) * angle) * value;
                if (i != 0)
                {
                    area += Math.Abs(aprev * b - bprev * a) / 2;
                }

                aprev = a;
                bprev = b;
                values.Points.Add(new DataPoint(value, angle));
                currAngles = nextangle;
            }

            AreaStrings[1] = "Площа: " + Math.Round(area, 2);
            values.Fill = OxyColor.FromArgb(200, 150, 186, 247);
            values.Points.Add(values.Points[0]);
            return values;
        }
        private AreaSeries CalculateProgrammingExpertChart()
        {
            var values = new AreaSeries() { Title = "Програмування" };

            int sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += _mainViewModel.WeightCoefficients[i].ProgrammingExpertCoef;
            }

            double a, b, aprev = 0, bprev = 0;
            double area = 0;
            double currAngles = 0 - ((double)_mainViewModel.WeightCoefficients[0].ProgrammingExpertCoef / sum) * 360 / 2;
            for (int i = 0; i < 10; ++i)
            {
                double angle = ((double)_mainViewModel.WeightCoefficients[i].ProgrammingExpertCoef / sum) * 360;
                double value = 
                    _mainViewModel.WeightCoefficients[i].ProgrammingExpertCoef 
                    * _mainViewModel.ExpertMarks[i].ProgrammingExpertMark 
                    * _mainViewModel.ExpertTypeWeights[2].RelativeWeightCoef;

                double nextangle = currAngles += angle;
                angle = (currAngles + nextangle) / 2;

                a = Math.Sin((Math.PI / 180) * angle) * value;
                b = Math.Cos((Math.PI / 180) * angle) * value;
                if (i != 0)
                {
                    area += Math.Abs(aprev * b - bprev * a) / 2;
                }

                aprev = a;
                bprev = b;
                values.Points.Add(new DataPoint(value, angle));
                currAngles = nextangle;
            }

            AreaStrings[2] = "Площа: " + Math.Round(area, 2);
            values.Fill = OxyColor.FromArgb(200, 7, 82, 186);
            values.Points.Add(values.Points[0]);
            return values;
        }
        private AreaSeries CalculateUsersChart()
        {
            var values = new AreaSeries() { Title = "Користувачі" };

            int sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += _mainViewModel.WeightCoefficients[i].PotentialUserCoef;
            }

            double a, b, aprev = 0, bprev = 0;
            double area = 0;
            double currAngles = 0 - ((double)_mainViewModel.WeightCoefficients[0].PotentialUserCoef / sum) * 360 / 2;
            for (int i = 0; i < 10; ++i)
            {
                double angle = ((double)_mainViewModel.WeightCoefficients[i].PotentialUserCoef / sum) * 360;
                double value =
                    _mainViewModel.WeightCoefficients[i].PotentialUserCoef 
                    * _mainViewModel.ExpertMarks[i].PotentialUserMark 
                    * _mainViewModel.ExpertTypeWeights[3].RelativeWeightCoef;

                double nextangle = currAngles += angle;
                angle = (currAngles + nextangle) / 2;

                a = Math.Sin((Math.PI / 180) * angle) * value;
                b = Math.Cos((Math.PI / 180) * angle) * value;
                if (i != 0)
                {
                    area += Math.Abs(aprev * b - bprev * a) / 2;
                }

                aprev = a;
                bprev = b;
                values.Points.Add(new DataPoint(value, angle));
                currAngles = nextangle;
            }

            AreaStrings[3] = "Площа: " + Math.Round(area, 2);
            values.Fill = OxyColor.FromArgb(200, 245, 135, 201);
            values.Points.Add(values.Points[0]);
            return values;
        }
        private AreaSeries CalculateAverageChart()
        {
            var values = new AreaSeries() { Title = "Усереднені" };

            double sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += _mainViewModel.WeightCoefficients[i].GetAverage();
            }

            double q = 
                _mainViewModel.ExpertTypeWeights[0].RelativeWeightCoef 
                + _mainViewModel.ExpertTypeWeights[1].RelativeWeightCoef 
                + _mainViewModel.ExpertTypeWeights[2].RelativeWeightCoef 
                + _mainViewModel.ExpertTypeWeights[3].RelativeWeightCoef;

            double a, b, aprev = 0, bprev = 0;
            double area = 0;
            double currAngles = 0 - (_mainViewModel.WeightCoefficients[0].GetAverage() / sum) * 360 / 2;

            for (int i = 0; i < 10; ++i)
            {
                double angle = ((double)_mainViewModel.WeightCoefficients[i].GetAverage() / sum) * 360;
                double value = _mainViewModel.WeightCoefficients[i].ProgrammingExpertCoef * _mainViewModel.ExpertMarks[i].ProgrammingExpertMark * _mainViewModel.ExpertTypeWeights[2].RelativeWeightCoef +
                               _mainViewModel.WeightCoefficients[i].ExpertCoef * _mainViewModel.ExpertMarks[i].AreaExpertMark * _mainViewModel.ExpertTypeWeights[0].RelativeWeightCoef +
                               _mainViewModel.WeightCoefficients[i].UsabilityExpertCoef * _mainViewModel.ExpertMarks[i].UsabilityExpertMark * _mainViewModel.ExpertTypeWeights[1].RelativeWeightCoef +
                               _mainViewModel.WeightCoefficients[i].PotentialUserCoef * _mainViewModel.ExpertMarks[i].PotentialUserMark * _mainViewModel.ExpertTypeWeights[3].RelativeWeightCoef;
                value /= q;

                double nextAngle = currAngles += angle;
                angle = (currAngles + nextAngle) / 2;

                a = Math.Sin((Math.PI / 180) * angle) * value;
                b = Math.Cos((Math.PI / 180) * angle) * value;
                if (i != 0)
                {
                    area += Math.Abs(aprev * b - bprev * a) / 2;
                }

                aprev = a;
                bprev = b;
                values.Points.Add(new DataPoint(value, angle));
                currAngles = nextAngle;
            }

            AreaStrings[4] = "Площа: " + Math.Round(area, 2);
            
            values.Fill = OxyColor.FromArgb(100, 255, 255, 183);
            values.Points.Add(values.Points[0]);
            return values;
        }
    }
}

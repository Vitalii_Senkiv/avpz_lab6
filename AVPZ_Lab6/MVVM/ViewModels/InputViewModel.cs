﻿using AVPZ_Lab6.Infrastructure.Commands;
using AVPZ_Lab6.MVVM.Models;
using AVPZ_Lab6.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;

namespace AVPZ_Lab6.MVVM.ViewModels
{
    internal class InputViewModel : ViewModelBase
    {
        #region Commands
        public LambdaCommand RandomizeCoefficientsCommands { get; set; }
        #endregion

        MainViewModel _mainViewModel;

        public ObservableCollection<WeightCoefficientsItem> WeightCoefficients
        {
            get => _mainViewModel.WeightCoefficients;
            set
            {
                _mainViewModel.WeightCoefficients = value;
                OnPropertyChanged(nameof(WeightCoefficients));
            }
        }
        public ObservableCollection<UserMarksItem> UserMarks
        {
            get => _mainViewModel.UserMarks;
            set
            {
                _mainViewModel.UserMarks = value;
                OnPropertyChanged(nameof(UserMarks));
            }
        }
        public ObservableCollection<ExpertMarksItem> ExpertMarks
        {
            get => _mainViewModel.ExpertMarks;
            set
            {
                _mainViewModel.ExpertMarks = value;
                OnPropertyChanged(nameof(ExpertMarks));
            }
        }
        public ObservableCollection<ExpertTypeWeightsItem> ExpertTypeWeights
        {
            get => _mainViewModel.ExpertTypeWeights; 
            set
            {
                _mainViewModel.ExpertTypeWeights = value;
                OnPropertyChanged(nameof(ExpertTypeWeights));
            }
        }

        public InputViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;

            WeightCoefficients = new ObservableCollection<WeightCoefficientsItem>();
            #region WeightCoefficients Setup
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Точність управління та обчислень", Id = 1 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Ступінь стандартності інтерфейсів", Id = 2 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Функціональна повнота", Id = 3 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Стійкість до помилок", Id = 4 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Можливість розширення", Id = 5 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Зручність роботи", Id = 6 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Простота роботи", Id = 7 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Відповідність чинним стандартам", Id = 8 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Переносимість між ПЗ", Id = 9 });
            WeightCoefficients.Add(new WeightCoefficientsItem { Name = "Точність управління та обчислень", Id = 10 });
            #endregion

            UserMarks = new ObservableCollection<UserMarksItem>();
            ExpertMarks = new ObservableCollection<ExpertMarksItem>();
            #region User and Expert Marks Setup
            for (int i = 1; i <= 10; i++)
            {
                UserMarks.Add(new UserMarksItem { Id = i });
                ExpertMarks.Add(new ExpertMarksItem { Id = i });
            }
            #endregion

            ExpertTypeWeights = new ObservableCollection<ExpertTypeWeightsItem>();
            #region ExpertTypeWeights Setup
            ExpertTypeWeights.Add(new ExpertTypeWeightsItem { Id = 1, Name = "Експерт з газузі" });
            ExpertTypeWeights.Add(new ExpertTypeWeightsItem { Id = 2, Name = "Експерт юзабіліті" });
            ExpertTypeWeights.Add(new ExpertTypeWeightsItem { Id = 3, Name = "Експерт з програмування" });
            ExpertTypeWeights.Add(new ExpertTypeWeightsItem { Id = 4, Name = "Потенційні користувачі" });
            #endregion

            RandomizeCoefficientsCommands = new LambdaCommand(o => 
            {
                RandomizeCoefficients();

                var tempWeightCoefficients = WeightCoefficients;
                WeightCoefficients = null;
                WeightCoefficients = tempWeightCoefficients;

                var tempUserMarks = UserMarks;
                UserMarks = null;
                UserMarks = tempUserMarks;

                var tempExpertMarks = ExpertMarks;
                ExpertMarks = null;
                ExpertMarks = tempExpertMarks;

                var tempExpertTypeWeight = ExpertTypeWeights;
                ExpertTypeWeights = null;
                ExpertTypeWeights = tempExpertTypeWeight;
            });
        }

        void RandomizeCoefficients()
        {
            foreach (var weightCoefsItem in WeightCoefficients)
            {
                weightCoefsItem.RandomizeExpertCoefficients();
            }

            foreach (var userMarksItem in UserMarks)
            {
                userMarksItem.RandomizeMarks();
            }

            for (int i = 0; i < ExpertMarks.Count; ++i)
            {
                ExpertMarks[i].RandomizeMarks(UserMarks[i].GetMarksSum());
            }

            foreach (var expertTypeWeight in ExpertTypeWeights)
            {
                expertTypeWeight.RandomizeCoefficients();
            }
        }
    }
}

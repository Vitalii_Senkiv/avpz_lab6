﻿using AVPZ_Lab6.Infrastructure.Commands;
using AVPZ_Lab6.MVVM.Models;
using AVPZ_Lab6.MVVM.ViewModels.Base;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace AVPZ_Lab6.MVVM.ViewModels
{
    internal class MainViewModel : ViewModelBase
    {
        #region CurrentView
        private object _currentView;
        public object CurrentView
        {
            get { return _currentView; }
            set { Set(ref _currentView, value); }
        }
        #endregion

        #region ViewModels
        public InputViewModel InputViewModel { get; set; }
        public ComplexQualityViewModel ComplexQualityViewModel { get; set; }
        public GraphicsViewModel GraphicsViewModel { get; set; }

        #endregion

        #region SwitchCommands
        public LambdaCommand SwitchToInputCommand { get; set; }
        public LambdaCommand SwitchToComplexQualityCommand { get; set; }
        public LambdaCommand SwitchToGraphicsCommand { get; set; }

        #endregion

        #region General Tables Data
        private ObservableCollection<WeightCoefficientsItem> _weightCoefficients;
        public ObservableCollection<WeightCoefficientsItem> WeightCoefficients
        {
            get => _weightCoefficients;
            set => Set(ref _weightCoefficients, value);
        }


        private ObservableCollection<UserMarksItem> _userMarks;
        public ObservableCollection<UserMarksItem> UserMarks
        {
            get => _userMarks;
            set => Set(ref _userMarks, value);
        }


        private ObservableCollection<ExpertMarksItem> _expertMarks;
        public ObservableCollection<ExpertMarksItem> ExpertMarks
        {
            get => _expertMarks;
            set => Set(ref _expertMarks, value);
        }


        private ObservableCollection<ExpertTypeWeightsItem> _expertTypeWeights;
        public ObservableCollection<ExpertTypeWeightsItem> ExpertTypeWeights
        {
            get => _expertTypeWeights;
            set => Set(ref _expertTypeWeights, value);
        }
        #endregion

        public MainViewModel()
        {
            InputViewModel = new InputViewModel(this);
            ComplexQualityViewModel = new ComplexQualityViewModel(this);
            GraphicsViewModel = new GraphicsViewModel(this);

            SwitchToInputCommand = new LambdaCommand(o => CurrentView = InputViewModel);
            SwitchToComplexQualityCommand = new LambdaCommand(o =>
            {
                CurrentView = ComplexQualityViewModel;
                ComplexQualityViewModel.CalculateTable();
            });
            SwitchToGraphicsCommand = new LambdaCommand(o =>
            {
                CurrentView = GraphicsViewModel;
                GraphicsViewModel.UpdateCharts();
            });

            CurrentView = InputViewModel;
        }
    }
}
